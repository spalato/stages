# Stages: classes and APIs for motion stages

Contains OO classes for translation stages. So far, we have only one: LTA-HS from Newport. This aims at providing a standard API when the need arises, so MDRIVE is also used as an inspiration.

## Contents

* Stages : Base utilities
* NewportXPS : Stages controlled through Newport's XPS motion controller.

## Requires

* XPS: Newport's XPS python module. It seems to be copyrighted, so this repo needs to stay private.

## See also

* XPS programming reference.
* MCODE reference: http://motion.schneider-electric.com/downloads/manuals/MCode.pdf
