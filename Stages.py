"""
stages.py  OO control of motion stages. Common stuff is here.

@author: Samuel Palato

Provides a standard API for the control of motion stages. So far, we own only a
Newport LTA-HS, so implementation will not be complete and design will not be
fully elegant.

This module contains objects useful for all submodules. Contains only base
classes and Cooperative MI classes. Since this module (tries to) use cooperative
MI, use keywords for method with complex callsigs (init, in particular).
"""


from __future__ import division

__author__ = 'Samuel Palato'
__all__ = ["StageError", "FakeStage", "LoggingStage", "SequenceStage",
           "ScanningStage"]
import time
import logging
from itertools import tee



logger = logging.getLogger(__name__)
#logger.setLevel(logging.DEBUG)



class StageError(Exception):
    """Base exception for the stages module."""
    pass


class MotionStage(object):
    """Base class for Motion Stages."""
    # TODO: do I want a "wait until finished"?

    def __init__(self):
        pass

    def move_abs(self, target):
        """Move to absolute position"""
        raise NotImplementedError

    def move_rel(self, delta):
        """Move relative to current position"""
        raise NotImplementedError

    def home(self):
        """Move to Home position, do home search"""
        pass

    def wait(self):
        """Wait for current motion to stop"""
        # TODO: maybe we want to have waiting moves (moveabs and wait)
        raise NotImplementedError

    @property
    def position(self):
        """Access current stage's position"""
        raise NotImplementedError


class FakeStage(MotionStage):
    """
    Fake stage does nothing. Simply updates position when requested to move.
    """
    # TODO: test TestStage(LoggingStage, FakeStage)
    # TODO: test TestScanningStage(LoggingStage, ScanningStage, TestStage)
    # TODO: check it is the same as TestSLStage(ScanningStage, LoggingStage, FakeStage)

    def __init__(self, wait_delay=0.5, **kw):
        super(FakeStage, self).__init__(**kw)
        self._position = 0  # fake position
        self.wait_delay = wait_delay

    def move_abs(self, target):
        self._position = target

    def move_rel(self, delta):
        self._position += delta

    def home(self):
        self._position = 0

    def wait(self):
        time.sleep(self.wait_delay)

    @property
    def position(self):
        return self._position


class LoggingStage(MotionStage):
    """
    Logging stage. Does nothing but log. Delegates the action to super()

    In general, you want to log both before and after action has taken place,
    so this class should go low on the MRO.
    """
    # TODO: setup custom formatter including stage ID
    def __init__(self, log_level=logging.INFO, **kw):
        self.log_level = log_level
        logger.log(self.log_level, "Initializing stage")
        super(LoggingStage, self).__init__(**kw)

    def move_abs(self, target):
        logger.log(self.log_level, "Absolute move to: %f", target)
        super(LoggingStage, self).move_abs(target)
        logger.log(self.log_level, "Move done")

    def move_rel(self, delta):
        logger.log(self.log_level, "Relative move by: %f", delta)
        super(LoggingStage, self).move_rel(delta)

    def home(self):
        logger.log(self.log_level, "Homing stage")
        super(LoggingStage, self).home()

    @property
    def position(self):
        pos = super(LoggingStage, self).position
        logger.log(self.log_level, "Stage position: %f", pos)
        return pos

    def wait(self):
        logger.log(self.log_level, "Waiting...")
        super(LoggingStage, self).wait()


class SequenceStage(MotionStage):
    """
    Sequence stage: Iterator that moves to predefined steps.

    Implements a blocking 'next' method that makes relative moves of a constant
    stepsize. Should go before concrete implementation in MRO.
    """
    def __init__(self, seq, **kw):
        """
        Setup the sequence stage.

        seq must be a sequence that contains the target positions.
        """
        super(SequenceStage, self).__init__(**kw)
        self.seq = seq

    def __iter__(self):
        # make a new copy of the iterator.
        self.seq, self._seq = tee(self.seq)  # keep copy of iterable for next loop
        return self

    def next(self):
        # we want to use the bottom of the MRO for that, emulate scripting usage
        self.move_abs(self._seq.next())
        self.wait()
        return self.position


class ScanningStage(MotionStage):
    """
    Scanning stage. Iterator that makes relative moves in a given range.

    Implements a blocking 'next' method that makes relative moves of a constant
    stepsize. Should go after concrete implementation in MRO.
    """
    def __init__(self, stepsize, start=None, end=None, **kw):
        """
        Setup the scanning stage.

        Each call to 'next' will make a relative move, from 'start' to 'end'.
        Both these limits can be 'None', in which case the stage starts from
        current position and moves indefinitely (ie: until another terminating
        condition is met.)

        Parameters
        ----------
        stepsize  float, size of relative move
        start     float, starting position
        end       float, end position
        """
        # !!! NOO! BUILD NUMBER OF STEPS AND ITERATE ON THAT
        super(ScanningStage, self).__init__(**kw)
        if stepsize == 0:
            raise ValueError("Can't scan with 0 stepsize.")
        self.stepsize = stepsize
        self.start = start
        self.end = end

    def __iter__(self):
        if self.start:
            super(ScanningStage, self).move_abs(self.start)
            super(ScanningStage, self).wait()
        # setup direction-dependant function to tell us if we're done
        # TODO: bad implementation, has error buildup: generate the sequence first and move to successive sequences.
        #
        if self.end is None:
            self.scan_finished = lambda s: False
        elif self.stepsize > 0:  # We are going in positive direction
            self.scan_finished = lambda s: s.position >= s.end
        else:  # We have an endpoint and going negative
            self.scan_finished = lambda s: s.position <= s.end
        return self

    def next(self):
        if self.scan_finished(self):
            raise StopIteration
        super(ScanningStage, self).move_rel(self.stepsize)
        super(ScanningStage, self).wait()
        return self.position # get position from the top of the MRO




