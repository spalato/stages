"""
NewportXPS.py  High level API for Newport XPS motion stages.

@author: Samuel Palato

Contains High-level OO wrappers over Newport's XPS_Q8_drivers.py python module.
"""

from __future__ import print_function

__author__ = 'Samuel Palato'
#__all__ = ['XPSGroup', 'SingleAxisGroup']

import inspect
from functools import wraps
import logging
logger = logging.getLogger(__name__)
from time import sleep
from Stages import StageError, MotionStage
from XPS_Q8_drivers import XPS


# XPS IP adress http://132.206.205.252/


class XPSError(StageError):
    """XPS call failed."""
    def __init__(self, errno, inst=None, socket=None):
        self.errno = errno
        if inst:
            self.msg = inst.ErrorStringGet(socket, errno)

    def __repr__(self):
        s = "XPSError("+repr(self.errno)
        if self.msg:
            s += ", msg="+repr(self.msg)
        s += ")"
        return s

    def __str__(self):
        return repr(self)


def handle_errors(func):
    """Decorator to handle Newport XPS errors.

    The call will now eat the error code and return the actual results, raising
    XPSErrors appropriately
    """
    #print("Decorating:", func.__name__)
    @wraps(func)
    def _err_handling(*args, **kwargs):
        self = args[0]
        socket = args[1]
        res = func(*args, **kwargs)
        try:
            err_code = res.pop(0)
        except AttributeError:
            if res is None:
                raise XPSError(None)  # TODO: check this. That means socket is not connected.
            else:
                assert isinstance(res, int)  # maybe too tight?
                err_code = res
        if err_code != 0:  # TODO: check this, plus there may be a message in `res`
            raise XPSError(err_code, self, socket)
        if len(res) == 1:
            res = res[0]
        else:
            res = tuple(res)
        if res != '':
            return res
    return _err_handling


class XPSController(object, XPS):
    """
    Basic wrapped around Newport supplied python drivers. Handles errors.
    """
    no_errh = ['__init__', '_XPS__sendAndReceive', 'TCP_ConnectToServer',
        'TCP_SetTimeout', 'TCP_CloseSocket', 'GetLibraryVersion']
    # TODO: check "get error message" and that sort of stuff.
    # next lines taken from: http://stackoverflow.com/questions/3467526/attaching-a-decorator-to-all-functions-within-a-class
    def __init__(self):
        XPS.__init__(self)

for name, fn in inspect.getmembers(XPS, inspect.isroutine):
    if not name in XPSController.no_errh:
        #print("handling", name)
        setattr(XPSController, name, handle_errors(fn))


class SingleAxisGroup(MotionStage):
    """
    Newport XPS single axis group.

    Needs an XPS Controller to make the requests.
    """
    # TODO: init, all moves, position
    def __init__(self, controller, ip, port, group_name, positionner_name,
                 force_home=False, wait_time=0, **kw):
        super(SingleAxisGroup, self).__init__(**kw)
        self.grp_name = group_name
        self.pos_name = group_name + '.' + positionner_name
        self._ctrlr = controller
        self._socket = self._ctrlr.TCP_ConnectToServer(ip, port, 1)
        self.wait_time = wait_time
        if force_home or not self.is_ready:
            if self.is_busy:
                if not force_home:
                    pass
                    #raise StageError("Trying to initialize a busy stage")
                else:
                    logger.warning("Killing an active stage.")
            self._ctrlr.GroupKill(self._socket, self.grp_name)
            self._ctrlr.GroupInitialize(self._socket, self.grp_name)
            self.home()

    def move_abs(self, target):
        self._ctrlr.GroupMoveAbsolute(self._socket, self.grp_name, [target])

    def move_rel(self, delta):
        self._ctrlr.GroupMoveRelative(self._socket, self.grp_name, [delta])

    def home(self):
        self._ctrlr.GroupHomeSearch(self._socket, self.grp_name)

    @property
    def position(self):
        return self._ctrlr.GroupPositionCurrentGet(self._socket, self.pos_name,
                                                   1)
    @property
    def status(self):
        return self._ctrlr.GroupStatusGet(self._socket, self.grp_name)

    @property
    def status_str(self):
        return self._ctrlr.GroupStatusStringGet(self._socket, self.status)

    @property
    def is_ready(self):
        return 'ready' in self.status_str.lower()

    @property
    def is_init(self):
        return not 'notinit' in self.status_str.lower()

    @property
    def is_disabled(self):
        return 'disable' in self.status_str.lower()

    @property
    def is_busy(self):
        val = ((not (self.is_ready or self.is_disabled)) and
               self.is_init and
               not self.status == 42) # this is NotReferenced
        return val

    def wait(self):
        sleep(self.wait_time)
